#!/bin/sh
set -e

####
echo "Construyendo los recursos"

echo ansible
docker image build --rm -t ansible:local ./docker/ansible/

echo terraform

docker image build --rm --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
                --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
                -t tf:local ./docker/terraform/

###
echo "Agregando alias a ~/.bashrc"

#ansible-playbook
echo 'alias ansible-playbook="docker container run --rm -it -v $PWD:/app:ro ansible:local ansible-playbook"' >> ~/.bashrc
echo 'alias terraform="docker container run --rm -it -u $(id -u):$(id -g) -v $PWD:/app tf:local"' >> ~/.bashrc

###
