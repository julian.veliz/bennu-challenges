#!/bin/sh

set -e

#source build_keys.sh
sh build_resources.sh

check_syntax () {
    docker run --rm -it -v $PWD:/app:ro ansible:local ansible-playbook --inventory-file="terraform/inventory" $1 --syntax-check
}

function_terraform () {

    docker run --rm -it -u $(id -u):$(id -g) -v $PWD:/app tf:local $1 $2
}
function_ansible () {

    docker run --rm -it -v $PWD:/app:ro ansible:local ansible-playbook --inventory-file="terraform/inventory" $1
}

validate_template () {

    function_terraform $1
}
###
cd terraform

validate_template init
validate_template validate
validate_template plan
function_terraform apply -auto-approve 

###
cd ..

#waiting for the hosts
sleep 60

for playbook in *.yml;
do
	check_syntax ${playbook}
done

function_ansible orquestator.yml
