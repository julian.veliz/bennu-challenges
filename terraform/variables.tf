variable "aws_region" {
  description = "Region de la nube"
  default = "us-east-1"
}

variable "vpc_cidr" {
  description = "Bloque CIDR para la VPC"
  default = "10.10.0.0/16"
}

variable "public_subnet_cidr" {
  description = "Bloque CIDR para subnet publica"
  default = "10.10.1.0/24"
}

variable "private_subnet_cidr" {
  description = "Bloque CIDR para subnet privada"
  default = "10.10.2.0/24"
}

variable "ami" {
  description = "Imagen AMI para la instancia EC2"
  default = "ami-02eac2c0129f6376b"
}

variable "key_path" {
  description = "Ruta de llave publica SSH"
  default = "/ssh/keypair.pub"
}

variable "var_ec2_type" {
  description = "Tipo de instancia EC2"
  default = "t2.micro"
}

