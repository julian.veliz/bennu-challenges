#AWS como proveedor
provider "aws" {
  region	= "${var.aws_region}"
}

#Instancia EC2
resource "aws_instance" "bennu" {
  ami		= "${var.ami}"
  instance_type = "${var.var_ec2_type}"
  key_name	= "${aws_key_pair.keypair.id}" 
  subnet_id 	= "${aws_subnet.public-subnet.id}"
  vpc_security_group_ids = ["${aws_security_group.sgweb.id}"]
  associate_public_ip_address = true
  source_dest_check = false

  tags {
    "Name"	= "bennu"
  }

  provisioner "local-exec" {
    command	= "echo ${aws_instance.bennu.public_ip} > inventory"
  }
}

output "ip" {
  value		= "${aws_instance.bennu.public_ip}"
}

resource "aws_key_pair" "keypair" {
  key_name = "keypair"
  public_key = "${file("${var.key_path}")}" 
}

