#Definicion de nuestra red VPC
resource "aws_vpc" "custom-vpc" {
  cidr_block = "${var.vpc_cidr}"

  tags {
    Name = "Custom VPC"
  }
}

#Subred publica
resource "aws_subnet" "public-subnet" {
  vpc_id = "${aws_vpc.custom-vpc.id}"
  cidr_block = "${var.public_subnet_cidr}"
  availability_zone = "us-east-1a"

  tags {
    Name = "Public Subnet"
  }
}

#Subred privada
resource "aws_subnet" "private-subnet" {
  vpc_id = "${aws_vpc.custom-vpc.id}"
  cidr_block = "${var.private_subnet_cidr}"
  availability_zone = "us-east-1b"

  tags {
    Name = "Private Subnet"
  }
}

#Internet gateway
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.custom-vpc.id}"

  tags {
    Name = "VPC IGW"
  }
}

#Tabla de rutas
resource "aws_route_table" "public-route" {
  vpc_id = "${aws_vpc.custom-vpc.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  tags {
    Name = "Public Subnet RT"
  }
}

#Tabla de rutas asignando a subred publica
resource "aws_route_table_association" "public-route-assoc" {
  subnet_id = "${aws_subnet.public-subnet.id}"
  route_table_id = "${aws_route_table.public-route.id}"
}

#Grupo de seguridad para subred publica
resource "aws_security_group" "sgweb" {
  name = "vpc_sg_web"
  description = "Allow incoming SSH connections"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  ingress {
    from_port = 5000
    to_port = 5000
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8000
    to_port = 8000
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  ingress {
    from_port = 8001
    to_port = 8001
    protocol = "tcp"
    cidr_blocks =  ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks     = ["0.0.0.0/0"]
  }

  vpc_id="${aws_vpc.custom-vpc.id}"

  tags {
    Name = "Web Server SG"
  }
}

