## Resolver los siguientes challenges
* **docker container run -it --rm bennu/jobs:sh**
* **docker container run -it --rm bennu/jobs:missing**


## |sh|zsh|ksh| all are a world
Ejecutando lo siguiente
```sh
$ docker container run -it --rm bennu/jobs:sh
```
Se obtuvo una respuesta de:
```sh
Status: Downloaded newer image for bennu/jobs:sh
/opt/jobs/sh/entrypoint.sh: line 14: $1: unbound variable
FIXME: Please send us what you found, contacto@bennu.cl.
```
* Se creo un contenedor de la imagen **bennu/jobs:sh** sin embargo el entrypoint falló.
* Nos indica que una variable no fue asignada a `$1` y el error se encuentra en la linea 14.
* Tampoco podemos revisar el log debido al parametro `--rm` en la ejecución.

Ejecute el contenedor sin el parametro de remove.
```sh
$ docker logs AAAAAAAAAAAA
$1: unbound variable
FIXME: Please send us what you found, contacto@bennu.cl.

$ docker history bennu/jobs:sh
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
deb26fd76217        6 days ago          /bin/sh -c #(nop)  ENTRYPOINT ["/opt/jobs/sh…   0B
<missing>           6 days ago          /bin/sh -c #(nop) COPY dir:f92cb182c0df79b2d…   480B
<missing>           7 days ago          /bin/sh -c #(nop) WORKDIR /opt/jobs/sh          0B
<missing>           7 days ago          /bin/sh -c apk add --no-cache bash              4.03MB
<missing>           3 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing>           3 weeks ago         /bin/sh -c #(nop) ADD file:2a1fc9351afe35698…   5.53MB

$ docker inspect AAAAAAAAAAAA
...
],
            "Cmd": null,
            "ArgsEscaped": true,
            "Image": "bennu/jobs:sh",
            "Volumes": null,
            "WorkingDir": "/opt/jobs/sh",
            "Entrypoint": [
                "/opt/jobs/sh/entrypoint.sh"
],
...
```
Conocemos ya al causante de esto: **/opt/jobs/sh/entrypoint.sh** ademas que tenemos el directorio de trabajo **/opt/jobs/sh**.
```sh
$ docker container run -it --entrypoint=/bin/bash bennu/jobs:sh

bash-4.4# ls -l /opt/jobs/sh/
total 12
-rw-r--r--    1 root     root           131 Feb 17 22:13 Dockerfile
-rwxr-xr-x    1 root     root           332 Feb 18 21:33 entrypoint.sh
-rw-r--r--    1 root     root            17 Feb 17 21:52 message


bash-4.4# cat Dockerfile
FROM alpine:3.9

RUN apk add --no-cache bash

WORKDIR /opt/jobs/sh

COPY . /opt/jobs/sh

ENTRYPOINT ["/opt/jobs/sh/entrypoint.sh"]


bash-4.4# cat message
Hello
From
Bennu


bash-4.4# cat entrypoint.sh
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

fixme() {
        if [ "$?" -eq "1" ]; then
                echo FIXME: Please send us what you found, contacto@bennu.cl.
        fi
}

run() {
        if [ "$1" == "HelloFromBennu" ]; then
                /bin/sh
        fi
}

trap fixme EXIT

msg=""
cat message |
while IFS= read -r line
do
        msg+=$line
done

run $msg
```

##### Revisando el script:

* Es necesario que la condicion `"$1" == "HelloFromBennu"` se cumpla para que nos retorne el **/bin/sh**.
* De acuerdo al loop el mensaje que se esta formando es **HelloFromBennu**.
* Sin embargo la variable parece no obtener el resultado correcto y lo envia a la funcion.
* Investigando un poco revise que los procesos ejecutados a traves de **`|`** se combierten en subshell, debido a esto la variable es local.
* Para evitar que se pierda el resultado o tome el valor global se deberian agregar comillas `"$msg"`

##### Aplicando la solución.
```sh
bash-4.4# ./entrypoint.sh
./entrypoint.sh: line 14: $1: unbound variable
FIXME: Please send us what you found, contacto@bennu.cl.
bash-4.4# vi entrypoint.sh
bash-4.4# ./entrypoint.sh
bash-4.4#
```
## Missing

```sh
$ docker container run -it --rm bennu/jobs:missing
```
```
Status: Downloaded newer image for bennu/jobs:missing
/usr/local/bin/entrypoint.sh: line 15: /usr/local/bin/missing: No such file or directory
FIXME: Please send us what you found, contacto@bennu.cl.
```
* Se creo un contenedor de la imagen **bennu/jobs:missing** sin embargo el entrypoint falló.
* Nos indica que el error se encuentra en la linea 15, y que falta un archivo o directorio denominado **`/usr/local/bin/missing`** .
* Tampoco podemos revisar el log debido al parametro `--rm` en la ejecución.

Ejecute el contenedor sin el parametro de remove.
```sh
$ docker logs BBBBBBBBBBBB
/usr/local/bin/entrypoint.sh: line 15: /usr/local/bin/missing: No such file or directory
FIXME: Please send us what you found, contacto@bennu.cl.

$ docker history bennu/jobs:missing
IMAGE               CREATED             CREATED BY                                      SIZE                COMMENT
3666993d4cef        8 days ago          /bin/sh -c #(nop)  ENTRYPOINT ["/usr/local/b…   0B
<missing>           8 days ago          /bin/sh -c rm /usr/local/bin/missing            0B
<missing>           8 days ago          /bin/sh -c #(nop) COPY file:5a3e52082be53c24…   216B
<missing>           8 days ago          /bin/sh -c #(nop) COPY file:7bc872dba95fb518…   1.38MB
<missing>           8 days ago          /bin/sh -c apk add --no-cache bash              4.03MB
<missing>           3 weeks ago         /bin/sh -c #(nop)  CMD ["/bin/sh"]              0B
<missing>           3 weeks ago         /bin/sh -c #(nop) ADD file:2a1fc9351afe35698…   5.53MB
```
Ok, en una capa de la imagen se ve que se elimino el recurso y al parecer es un ejecutable **`rm /usr/local/bin/missing`**.
```sh
$ docker container run -it --entrypoint=/bin/bash bennu/jobs:missing
bash-4.4# ls -l /usr/local/bin/
total 4
-rwxr-xr-x    1 root     root           216 Feb 18 07:01 entrypoint.sh


bash-4.4# cat /usr/local/bin/entrypoint.sh
#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

fixme() {
        if [ "$?" -ne "0" ]; then
                echo FIXME: Please send us what you found, contacto@bennu.cl.
        fi
}

trap fixme EXIT

/usr/local/bin/missing
```
##### Revisando el script:

* Es necesario que la condicion `"$?" -ne "0"` se cumpla y nos entregue el **sh**.
* Sin embargo el archivo fue eliminado y es por ello que el resultado termina con error.
* Debemos crear un archivo y con permiso de ejecución para que pueda lanzarse.

##### Aplicando la solución.
```sh
bash-4.4# cd /usr/local/bin/
bash-4.4# ./entrypoint.sh
./entrypoint.sh: line 15: /usr/local/bin/missing: No such file or directory
FIXME: Please send us what you found, contacto@bennu.cl.
bash-4.4# touch missing
bash-4.4# chmod +x missing
bash-4.4# ./entrypoint.sh
bash-4.4#
```
