# Bennu Challenges

El siguiente repositorio trata sobre exponer una aplicación con el uso de las herramientas de aprovisionamiento  a traves de un api gateway [Kong](https://konghq.com/) y nos brinda un monton de opciones para gestionar nuestros servicios y lo bueno es que es opensource.

## Requisitos
### Terraform Provider AWS

Configurar `AWS_ACCESS_KEY_ID` y `AWS_SECRET_ACCESS_KEY`, como variables de entorno:

```sh
$ export AWS_ACCESS_KEY_ID="YOUR ACCESS KEY ID"
$ export AWS_SECRET_ACCESS_KEY="YOUR SECRET ACCESS KEY"
```
### Docker Engine
Docker Engine es posible obtenerlo aquí [docker-ce](https://hub.docker.com/search/?type=edition&offering=community), para comprobar use:

```sh
$ docker info
$ docker --version
```
#### binarios
* ssh-keygen
* rsync


## Uso
La estructura definida es la siguiente:
```bash
.
├── SCRIPTS.sh
├── ...
│
├── docker
│   ├── ansible
│   │   ├── Dockerfile
│   │   └── ...
│   └── terraform
│       └── ...
│
├── flask
│       ├── Dockerfile
│       ├── app.py
│	└── ...
│
├── PLAYBOOKS.yml
├── ...
│
├── roles
│   ├── role-for-app
│   │   ├── defaults
│   │   │   └── main.yml
│   │   ├── tasks
│   │       └── ...
│   ├── role-for-database
│   └── ...
│           └── ...
│
└── terraform
    ├── templates.tf
    ├── INVENTORY
    └── ...
```
* Se encuentran los `SCRIPTS.sh` que ejecutan ciertas tareas de creacion de recursos etc.
* La carpeta `docker` tiene los dockerfiles de los recursos. Ej. es necesario contar con los binarios de **ansible-playbook** y **terraform** es aquí donde se facilitan.
* La aplicación en `Flask` que sirve de source para conectarlo a **Kong**.
* La carpeta `roles` contiene los tareas definidas para nuestros propositos
* Los `PLAYBOOKS.yml` nos permiten ejecutar los **roles** definidos.
* En la carpeta `terraform` se encuentran los templates y donde va a ser generado el **INVENTORY** que usaran nuestros playbooks.

## Inicio rápido
Este script ejecutará los recursos en AWS y expondra el contenido de la aplicación Flask en Kong.
```sh
# Creamos los keys
$ sh build_keys.sh
# Lanzamos
$ sh run.sh
```
El recurso se mostrará en la ip de tu recurso creado que se encuentra en **terraform/inventory**
```sh
$ cat terraform/inventory 
11.22.33.44
```
## Comprobar
La aplicación **Flask** se expone en el puerto `5000` y el servicio **Kong** en el puerto `8000`.
* Para comprobar en Flask: [http://IP_INVENTORY:5000/api/request](http://IP_INVENTORY:5000/api/request)
* Para comprobar en Kong: [http://IP_INVENTORY:8000/api/request](http://IP_INVENTORY:8000/api/request)

