#!/bin/bash

set -e

value="keypair"

if [ -e "${value}" ]
then
       echo ERROR: "la llave ya fue creada"
       exit
fi

echo "Creando llaves SSH para las conexiones"

create_ssh_key () {
        ssh-keygen -t rsa -N "" -f $1
}

rsync_keys () {
        for x in `find ./docker -type d -printf '%P\n'`;
        do
                rsync ${value}* docker/$x/
        done
}

create_ssh_key ${value}
rsync_keys

