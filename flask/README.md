### Imagen docker de la aplicacion en Flask
Construir la imagen:
```sh
docker image build --rm -t flask:local .
```
Para correr la aplicación usar:
```sh
docker container run -d --name flask -p 5000:5000 flask:local
```
Para validar la url ingresar a [http://localhost:5000](http://localhost:5000) o desde consola:

```sh
curl -i http://localhost:5000
```
