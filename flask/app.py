#api rest usando flask
from flask import Flask,jsonify, make_response, request

app = Flask(__name__)

@app.route("/", methods=['GET'])
def hello():
    return jsonify({'msg':"HelloWorld"})

@app.route("/api/request", methods=['GET'])
def msg():
    if request.method == 'GET':
        return jsonify({'msg':"HelloWorld from APi"})

@app.route('/api/msg', methods=['GET'])
def status():
    if request.method == 'GET':
        return jsonify({'status_code':200})

if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0' , port=5000)

