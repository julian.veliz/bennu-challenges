### Imagen docker para Ansible
Construir la imagen:

```sh
docker image build --rm -t ansible:local .
```
Para usar la imagen ejecutar posicionarse en la carpeta de playbooks:
```sh
docker container run --rm -it -v $PWD:/app:ro ansible:local <command>
```
ejemplo
```sh
docker container run --rm -it -v $PWD:/app:ro ansible:local ansible --version
docker container run --rm -it -v $PWD:/app:ro ansible:local ansible-playbook --version
```
También pueden agregar alias para que sea mas simple:
```sh
#ansible
alias ansible="docker container run --rm -it -v $PWD:/app:ro ansible:local ansible"
#ansible-playbook
alias ansible-playbook="docker container run --rm -it -v $PWD:/app:ro ansible:local ansible-playbook"
```
Luego ya pueden usar:
```sh
ansible --version
ansible-playbook --version
```

