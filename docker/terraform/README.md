### Crear recursos con Terraform

Construir la imagen:

```sh
docker image build --rm --build-arg AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
                --build-arg AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
                -t tf:local .
```

Una vez construido usar de la sgte manera:

```sh
docker container run --rm -it -u $(id -u):$(id -g) -v $PWD:/app tf:local <command>
```
* `command` pueden ser (validate, plan, apply, show, destroy, etc.)
* Para el uso cotidiano se recomienda crear un `alias`:

```sh
alias terraform="docker container run --rm -it -u $(id -u):$(id -g) -v $PWD:/app tf:local"
```
Luego ya es posible usarlo simple:
```sh
terraform --version
```
**Nota:** asegurense ejecutar desde la carpeta `terraform` para mantener los estados.
